#ifndef FRACTION_H
using namespace std;
#include <iostream>
#define FRACTION_H


class Fraction {
	private:
        long long numerator;
        long long denominator;
    public:
		Fraction():numerator(0),denominator(1){};
		Fraction(long long num,long long denom):numerator(num),denominator(denom){};
    ~Fraction() {};
    static long long gcd(long long a,long long b);
    static long long lcm(long long a,long long b);
    friend std::ostream& operator<< (ostream &out, Fraction &fract); // �������� ������
    friend std::istream& operator>> (std::istream &in, Fraction &fract); //�������� �����
		Fraction operator+(Fraction a);
		Fraction operator-(Fraction a);
		Fraction operator*(Fraction a);
		Fraction operator/(Fraction a);
		bool operator==(Fraction a);
		bool operator!=(Fraction a);
		bool operator>(Fraction a);
		bool operator<(Fraction a);
		//bool operator < (Fraction& first, Fraction& second);
		//friend bool operator>=(const Fraction& first,const Fraction& second);
		//friend bool operator<=(const Fraction& first,const Fraction& second);
		long long getNumerator();
		long long getDenominator();
		void setNumerator(long long num);
		void setDenominator(long long num);
		Fraction Abs(Fraction fr);
};


#endif
