#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include "fraction.h"
#define ARR_SIZE 1001
using namespace std;

// gpp -Wall main.cpp fractions.cpp fractions.h -o main -std=c++14

bool menu ();		// Shows menu dialog
void firstLab ();		// The solution of systems of linear equations by the Jordan-Gauss method
void secondLab ();		// Finding basic and support solutions of systems of linear equations
long long fact (long long n);	// Calculating factorial
bool NextSet (int *a, int n, int m);
void print (Fraction ** A, int n, int m);
void JordanGauss (Fraction ** arr, int n, int m);
void JordanGaussTwo (Fraction ** arr, int n, int m);
long long combinationNum (long long a, long long b);
void rowSwap (Fraction ** arr, int n, int from, int to);
void copy (Fraction ** from, Fraction ** to, int n, int m);
void divRow (Fraction ** A, int n, int i, int baseElement);
void rowToZero (Fraction ** A, int m, int baseElement, int i);

int main (int argc, char *argv[]) {
    while (menu ());
    return 0;
}

bool NextSet (int *a, int n, int m) {
    int k = m;
    for (int i = k - 1; i >= 0; --i)
        if (a[i] < n - k + i + 1) {
            ++a[i];
            for (int j = i + 1; j < k; ++j)
                a[j] = a[j - 1] + 1;
            return true;
        }
    return false;
}

void print (Fraction ** A, int n, int m) {
    cout << endl;
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (j == (n - 1))
                printf (" | ");
            cout << setw (8) << A[i][j];
        }
        cout << endl;
    }
    //cout << endl;
}

void divRow(Fraction ** A, int n, int i, int baseElement){
    Fraction kk = A[baseElement][i], zero (0, 1);
    for (int j = i + 1; j < n; j++) {
        if (A[baseElement][j] != zero)
            A[baseElement][j] = A[baseElement][j] / kk;
        else
            A[baseElement][j] = A[baseElement][j] / kk;
    }
}


void rowToZero (Fraction ** A, int m, int baseElement, int i){
    Fraction kk (0, 1);
    for (int j = 0; j < m; j++) {
        if (j != baseElement)
            A[j][i] = kk;
    }
}


void rowSwap (Fraction ** arr, int n, int from, int to) {
    Fraction c;
    for (int i = 0; i < n; i++) {
        c = arr[from][i];
        arr[from][i] = arr[to][i];
        arr[to][i] = c;
    }
}

void JordanGauss (Fraction ** arr, int n, int m) {
    int i, j, y = 0, max_ind = 0, zz = 0;
    bool check;
    Fraction mx, dx;
    int basicIndexes[100][2] = { };

    for (i = 0; i < n; i++) {
        if (y > m - 1 || i == n - 1)
            break;
        check = false;
        max_ind = y;
        mx = Fraction (0, 1);
        /* #1 Searching maximum element in column */
        for (j = y; j < m; j++) {
            if (dx.Fraction::Abs (arr[j][i]) > mx) {
                mx = dx.Fraction::Abs (arr[j][i]);
                max_ind = j;
                check = true;
            }
        }
        /* #1 Searching maximum element in column */
        rowSwap (arr, n, y, max_ind);
        if (!check)
            continue;
        print (arr, n, m);

        /* #2 Recalculation of elements by the method of rectangles ignore the reference line */
        for (j = i + 1; j < n; j++)
            for (int l = 0; l < m; l++)
                if (l != y)
                    arr[l][j] = arr[l][j] - (arr[y][j] * arr[l][i]) / arr[y][i];
        /* #2 Recalculation of elements by the method of rectangles ignore the reference line */
        divRow (arr, n, i, y);
        rowToZero (arr, m, y, i);
        print (arr, n, m);
        basicIndexes[zz][0] = y;
        basicIndexes[zz][1] = i;
        zz++;
        y++;
    }

    /* #3 Print array of basicIndexes */
    for (i = 0; i < zz; i++) {
        cout << "basicIndexes[" << i << "][0] = " << basicIndexes[i][0];
        cout << " | basicIndexes[" << i << "][1] = " << basicIndexes[i][1] << endl;
    }
    /* #3 Print array of basicIndexes */
    Fraction result;
    int onesCounter = 0, co = 0, zeroesCnt;

    /* #4 Calculating count of ones and cheking lack of solutions */
    for (i = 0; i < m; i++) {
        onesCounter = 0;
        zeroesCnt = 0;
        for (j = 0; j < n - 1; j++) {
            if (arr[i][j] == Fraction (1, 1))
                onesCounter++;
            if (arr[i][j] == Fraction (0, 1))
                zeroesCnt++;
        }
        if (onesCounter == 1)
            co++;
        if (zeroesCnt == n - 1 && !(arr[i][n - 1] == Fraction (0, 1))) {
            printf ("\n There are not resolves! \n");
            return;
        }
    }

    /* #4 Calculating count of ones and cheking lack of solutions */
    if (co == m && co == n - 1) {
        for (i = 0; i < m; i++) {
            cout << "x" << i + 1 << " = " << arr[i][n - 1] << endl;
        }
    } else {
        for (i = 0; i < zz; i++) {
            cout << "x" << basicIndexes[i][1] + 1 << " = ";
            for (j = 0; j < n - 1; j++) {
                if (j != basicIndexes[i][1]) {
                    result = Fraction (-1, 1) * arr[basicIndexes[i][0]][j];
                    if (result > Fraction (0, 1))
                        cout << "+";
                    else if (result == Fraction (0, 1))
                        continue;
                    cout << result << "[x" << (j + 1);
                }
            }
            if (arr[i][n - 1] > Fraction (0, 1))
                printf ("+");
            if (!(arr[i][n - 1] == Fraction (0, 1)))
                cout << arr[i][n - 1] << endl;
        }
    }
}


void firstLab() {
    int i, j, n, m;
    Fraction recalc, tempFrac (0, 1);
    printf ("Input n,m, matrix: ");
    scanf ("%d %d", &n, &m);
    Fraction ** A = new Fraction *[m];
    for (int i = 0; i < m; i++)
        A[i] = new Fraction[n];
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            cin >> A[i][j];
    print(A, n, m);
    JordanGauss(A, n, m);
}


void secondLab() {
    int i, j, n, m;
    Fraction recalc, tempFrac (0, 1);
    printf ("Input n,m, matrix: ");
    scanf ("%d %d", &n, &m);
    Fraction ** A = new Fraction *[m];
    for (int i = 0; i < m; i++)
        A[i] = new Fraction[n];
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            cin >> A[i][j];
    print (A, n, m);
    JordanGaussTwo(A, n, m);
}



bool menu (){
    int n;
    printf
    ("\n1) The solution of systems of linear equations by the Jordan-Gauss method\n");
    printf
    ("2) Finding basic and support solutions of systems of linear equations\n");
    printf ("3) Generate Sochet\n");
    scanf ("%d", &n);
    switch (n) {
    case 1:
        firstLab();
        break;

    case 2:
        secondLab();
        break;

    default:
        printf("\nYour input is wrong!\n");
        break;
    }
    return true;
}


long long fact(long long n) {
    if (n <= 1)
        return 1;
    return n * fact (n - 1);
}

long long combinationNum(long long a, long long b){
    if (a == b)
        return 1;

    return fact (b) / (fact (b - a) * fact (a));
}


void JordanGaussTwo(Fraction ** arr, int n, int m) {
    int i, j, y = 0, mx_ind = 0;
    bool check;
    Fraction mx, dx;

    for (i = 0; i < n; i++) {
        if (y > m - 1 || i == n - 1)
            break;
        check = false;
        mx_ind = y;
        mx = Fraction (0, 1);

        /* #1 Searching maximum element in column */
        for (j = y; j < m; j++) {
            if (dx.Fraction::Abs (arr[j][i]) > mx) {
                mx = dx.Fraction::Abs (arr[j][i]);
                mx_ind = j;
                check = true;
            }
        }
        /* #1 Searching maximum element in column */
        rowSwap(arr, n, y, mx_ind);
        if (!check)
            continue;

        /* #2 Recalculation of elements by the method of rectangles ignore the reference line */
        for (j = i + 1; j < n; j++) {
            for (int l = 0; l < m; l++) {
                if (l != y)
                    arr[l][j] = arr[l][j] - (arr[y][j] * arr[l][i]) / arr[y][i];
            }
        }
        /* #2 Recalculation of elements by the method of rectangles ignore the reference line */

        /* #3 Div row on base element */
        for (j = i + 1; j < n; j++) {
            arr[y][j] = arr[y][j] / arr[y][i];
        }

        arr[y][i] = arr[y][i] / arr[y][i];
        /* #3 Div row on base element */

        /* #4 Column to zero */
        for (j = 0; j < m; j++) {
            if (j != y)
                arr[j][i] = Fraction (0, 1);
        }
        /* #4 Column to zero */

        y++;

    }

    /* #5 Rang Calculating */
    int rang = 0;
    int cnt = 0;
    for (i = 0; i < m; i++) {
        cnt = 0;
        for (j = 0; j < n - 1; j++) {
            if (arr[i][j] == Fraction (1, 1))
                cnt++;
        }
        if (cnt == 1)
            rang++;
    }
    m = rang;
    /* #5 Rang Calculating */

    print (arr, n, m);

    /* #6 Generating all combinations */
    int combNum = combinationNum(rang, n - 1);
    int comb_ind = rang;
    int *tmp_comb = new int[rang];
    int *comb = new int[rang * combNum];
    for (i = 0; i < rang; i++){
        tmp_comb[i] = i + 1;
        comb[i] = i + 1;
    }

    while (NextSet (tmp_comb, n - 1, rang)) {
        for (i = 0; i < rang; i++){
            comb[comb_ind] = tmp_comb[i];
            comb_ind++;
        }
    }
    /* #6 Generating all combinations */

    cout << "Rang :" << rang << endl << "N: " << n -
         1 << endl << "Number of combinations: " << combNum << endl;


    Fraction ** temp = new Fraction *[m];
    int ed[rang][n] = {};
    int ch0[rang] = {};
    Fraction result;
    bool flag1, flag2, flagO;
    int counter = 0;

    for (i = 0; i < m; i++)
        temp[i] = new Fraction[n];

    copy(arr, temp, n, m);
    print (temp, n, m);
    cout << endl;
    int tempH=0;
    for (i = 0 ; i < rang * combNum; i++) {
      cout << comb[i] << " ";
      if(tempH==rang-1) {
        cout << endl;
        tempH=0;
      }
      else {
        tempH++;
      }
    }


    cout << endl;
    for (i = 0; i < rang; i++)
        ch0[i] = 0;

    for (int k = 0; k < rang * combNum; k++) { // Cycle to through all combinations
        int index = comb[k];
        flag1 = 0;
        flag2 = 0;
        flagO = 1;
        counter = 0;
        for (i = 0; i < rang; i++) {
            if (!(temp[i][index - 1] == Fraction (0, 1)) && ch0[i] == 0) {
                ch0[i] = 1;
                flag1 = 1;
                /* #7 Recalculation of elements by the method of rectangles ignore the reference line */
                for (j = 0; j < rang; j++) {
                    for (int l = 0; l < n; l++) {
                        if (i == j || index - 1 == l)
                            continue;
                        temp[j][l] = temp[j][l] - (temp[j][index - 1] * temp[i][l]) / temp[i][index - 1];
                    }
                }
                /* #7 Recalculation of elements by the method of rectangles ignore the reference line */

                /* #8 Div column on zero and div row on base element */
                for (j = 0; j < rang; j++) {
                    if (j != i)
                        temp[j][index - 1] = Fraction (0, 1);
                }
                for (j = 0; j < n; j++) {
                    if (j != index - 1)
                        temp[i][j] = temp[i][j] / temp[i][index - 1];

                }
                /* #8 Div column on zero and div row on base element */
                temp[i][index - 1] = Fraction (1, 1);
                ed[i][index - 1] = 1;
                counter++;
                print(temp, n, m);
            }

            if (flag1)
                break;
            if (i == rang - 1 && counter != rang)
                flag2 = 1;
        }
        if((k + 1) % rang == 0) {
            for (i = 0; i < rang; i++)
                ch0[i] = 0;
            cout << endl << "Basic Resolving #" << ((k - 1) / rang) + 1 << " ";
            cout << "[";
            for (i = 0; i < rang; i++)
                cout << comb[k - rang + i + 1] << ",";
            cout << "]";
            cout << endl;
            if (flag2 == 1) {
                cout << "No basic resolve";
                copy (arr, temp, n, m);
                cout << endl << "_________________________________________________________________________________" << endl;
                continue;
            }

            for (j = 0; j < n - 1; j++) {
                result = Fraction (0, 1);
                for (i = 0; i < rang; i++) {
                    if (ed[i][j] == 1)
                        result = temp[i][n - 1];
                    if (result < Fraction (0, 1))
                        flagO = 0;
                }
                if (!(result == Fraction (1, 1)))
                    cout << setw (3) << result << "; ";
                else
                    cout << 0 << " ";
            }

            if (!flagO)
                cout << "Ne ";
            cout << "opornoe!";
            cout << endl << "_________________________________________________________________________________" << endl;
            for (i = 0; i < rang; i++) {
                for (j = 0; j < n; j++) {
                    ed[i][j] = 0;
                }
            }
            copy (arr, temp, n, m);
        }
    }

}

void copy (Fraction ** from, Fraction ** to, int n, int m){
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            to[i][j] = from[i][j];
}
