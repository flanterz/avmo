#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include "fraction.h"
#define ARR_SIZE 1001
using namespace std;
// 2 3 2 6 2 4 26 > 3 11 54 > 5 3 30 >
// gpp -Wall main.cpp fractions.cpp fractions.h -o main -std=c++14

bool menu ();		// Shows menu dialog
void firstLab ();		// The solution of systems of linear equations by the Jordan-Gauss method
void secondLab ();		// Finding basic and support solutions of systems of linear equations
void DualSimplexMethod();
long long fact (long long n);	// Calculating factorial
bool NextSet (int *a, int n, int m);
void print (Fraction ** A, int n, int m);
void JordanGauss (Fraction ** arr, int n, int m);
void JordanGaussTwo (Fraction ** arr, int n, int m);
long long combinationNum (long long a, long long b);
void rowSwap (Fraction ** arr, int n, int from, int to);
void copy (Fraction ** from, Fraction ** to, int n, int m);
void divRow (Fraction ** A, int n, int i, int baseElement);
void rowToZero (Fraction ** A, int m, int baseElement, int i);
Fraction Abs(Fraction fr) {
    Fraction c = fr;
    if (c.getNumerator() < 0)
        c.setNumerator(c.getNumerator()* (-1) );

    return c;
}
int main (int argc, char *argv[]) {
    DualSimplexMethod();
    //while (menu ());
    return 0;
}

bool NextSet (int *a, int n, int m) {
    int k = m;
    for (int i = k - 1; i >= 0; --i)
        if (a[i] < n - k + i + 1) {
            ++a[i];
            for (int j = i + 1; j < k; ++j)
                a[j] = a[j - 1] + 1;
            return true;
        }
    return false;
}

void print (Fraction ** A, int n, int m) {
    cout << endl;
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (j == (n - 1))
                printf (" | ");
            cout << setw (8) << A[i][j];
        }
        cout << endl;
    }
    //cout << endl;
}

void divRow(Fraction ** A, int n, int i, int baseElement) {
    Fraction kk = A[baseElement][i], zero (0, 1);
    for (int j = i + 1; j < n; j++) {
        if (A[baseElement][j] != zero)
            A[baseElement][j] = A[baseElement][j] / kk;
        else
            A[baseElement][j] = A[baseElement][j] / kk;
    }
}

void rowToZero (Fraction ** A, int m, int baseElement, int i) {
    Fraction kk (0, 1);
    for (int j = 0; j < m; j++) {
        if (j != baseElement)
            A[j][i] = kk;
    }
}

void rowSwap (Fraction ** arr, int n, int from, int to) {
    Fraction c;
    for (int i = 0; i < n; i++) {
        c = arr[from][i];
        arr[from][i] = arr[to][i];
        arr[to][i] = c;
    }
}

void DualSimplexMethod() {
    system("clear");
    ifstream fin("input.txt");
    int n, m, i, j;
    fin >> n >> m; //2 3
    Fraction A[m][n+1], B[m+1][n+m+1];
	Fraction Z[n], O(0, 1), E(1,1), ME(-1,1);
    char Achar[m];

    cout << "_________________________________________________________________" << endl;
    cout << "Input" << endl << endl;
    // #1 Z-row
    for(i=0; i<n; i++) fin >> Z[i];
    cout << "Z = ";
    for(i=0; i<n; i++) {
        if(i!=0 && Z[i] > O) cout << " + ";
        cout << Z[i] << "[x" << i+1 << "]";
    }
    cout << " -> min" << endl;
    
    // #2 Input system
    for(i=0; i<m; i++) {
        for(j=0; j<=n+1; j++) {
            if(j==n+1) fin >> Achar[i]; //////////////////////////////////////////////delete??
            else fin >> A[i][j];
        }
    }
    // #3 Output biginning system
    cout << endl;
    for(i=0; i<m; i++) {
        for(j=0; j<=n; j++) {
            //Free x >=
            if(j==n) {
                if(Achar[i] == '>') ////////////////////////////////////////////
                    cout << " >= " << A[i][j];
                else
                    cout << " <= " << A[i][j]; ////////////////////////////////////////////
            } else {
                if(A[i][j] > O) {
                    if(j!=0) cout << " + ";
                }
                cout << setw(2) << A[i][j] << "[x" << j+1 << "]";
            }
        }
        cout << endl;
    }
    cout << endl << "__________________________________________________________________________________" << endl;


    // #4 change to <= *(-1).
//    cout << "[ 2. Приведем систему ограничений к системе неравенств смысла ⩽ , умножив соответствующие строки на (-1) ]" << endl;
    for(i=0; i<m; i++) {
        for(j=0; j<=n; j++) {
            if(Achar[i] == '>') {
                A[i][j] = A[i][j]*ME;

            }
        }
//ob'edinit'        
			if(Achar[i] == '>') {
            Achar[i]='<';
        }
    }
    // #5 Output after changing
    cout << endl;
    for(i=0; i<m; i++) {
        for(j=0; j<=n; j++) {
            //Output free x <=
            if(j==n) {
                if(Achar[i] == '>')
                    cout << " >= " << A[i][j];
                else
                    cout << " <= " << A[i][j];
            } else {
                if(A[i][j] > O) {
                    if(j!=0) cout << " + ";
                }
                cout << setw(4) << A[i][j] << "[x" << j+1 << "]";
            }
        }
        cout << endl;
    }
    cout << endl << "___________________________________________________________________________" << endl;
//?????????????????????????????????????????????????????????????????????????????????????
    // #6 To canonian form 
    int mb = n;
    for(i=0; i<m; i++) {
        for(j=0; j<n+m+1; j++) {
            if(j<n) {
                B[i][j] = A[i][j];
            } else {
                if(j==mb)
                    B[i][j] = E;
                else
                    B[i][j] = O;
            }
        }
        mb++;
    }
    for(i=0; i<m; i++) {
        B[i][n+m] = A[i][n];
    }
    // #7 Output canonian form ==
    cout << endl;
    for (i=0; i<m; i++) {
        for (j=0; j<n+m+1; j++) {
            if (j==n+m) {
                if (Achar[i] == '<')
                    cout << " = " << B[i][n+m];
            } 
			else {
                if(B[i][j] > O) {
                    if(j!=0) cout << " + "; //obiedinit
                } else if(B[i][j]==O) {
                    if(j!=0) cout << " + ";
                }
                cout << setw(3) << B[i][j] << "[x" << j+1 << "] ";
            }
        }
        cout << endl;
    }

    // #8 Concanate B[][] & Z, *(-1)
    for(j=0; j<=n+m+1; j++) {
        if(j<n) {
            B[m][j] = Z[j]*ME;
        } 
		else {
            B[m][j] = O;
        }
    }
    cout << endl << "________________________________________________________________________________" << endl;

    // #9 Simplex Table
    int saveRow[m]; // SaveRow for #x of basic X
    for(i=0; i<=m; i++) {
        for(j=0; j<n+m+1; j++) {
            if(B[i][j] == E) { //� ������ ��� ����� ���� ���� �����, ��??
                saveRow[i] = j;
            }
        }
    }
    cout << endl << "SymplexTable: "<<  endl << endl << setw(4) << "�.�." << setw(7) << " |  1  | ";
    for(i=0; i<n+m; i++) {
        cout <<setw(4)<< "x" << i+1 << " | ";
    }
    cout << endl;
    for(i=0; i<=m; i++) {
        for(j=-2; j<n+m; j++) {
            if(j==-2) {
                if(i==m)cout << setw(4) << "Z " << " |";
                else cout << setw(3) << "x" << saveRow[i]+1 << " |";
            } else if(j==-1) {
                cout << setw(4) <<  B[i][n+m] << "| ";
            } else {
                cout << setw(5) <<  B[i][j] << " | ";
            }
        }
        cout << endl;
    }
    // #10 Max |otricatelniy|
    Fraction maxFraction = O;
    int maxIndex;
    for(i=0; i<n; i++) {
        if(Abs(B[i][m+n]) > maxFraction) {
            maxFraction = Abs(B[i][m+n]);
            maxIndex = i;
        }
    }
    cout <<"MaxRow: "<< maxIndex+1<< endl;
    // #11 New basic variable
    Fraction minOtn(100,1), T(1,1);
    int rc;
    for(j=0; j<n+m; j++) {
        if(B[m][j] == O) continue;
        T = B[m][j]/B[maxIndex][j];
        if(T < minOtn) {
            minOtn = T;
            rc = j;
        }
    }
    cout << "Min SO : "<< minOtn << endl;
    cout << "RC: "<< razStolbec+1 << endl;
    cout << "RW: "<< maxIndex+1 << endl;

    // #12 Arr B to Arr C
    Fraction C[m+1][n+m+1], Ct[m+1][n+m+1];
    for (i=0; i<=m; i++)  C[i][0] = B[i][n+m];
    for (i=0; i<=m; i++) {
        for (j=0; j<n+m; j++) {
            C[i][j+1] = B[i][j];
        }
    }
    C[3][6] = O; //?????????????????????????????????????????????????????????????

    // #13 Output table
    cout << endl << "Arr C: "<<  endl << setw(4) << "�.�." << setw(7) << " |  1  | ";
    for(i=0; i<n+m; i++) {
        cout <<setw(4)<< "x" << i+1 << " | ";
    }
    cout << endl;
    for(i=0; i<=m; i++) {
        for(j=0; j<n+m+1; j++) {
            if(j==0) {
                if(i==m)
                    cout << setw(4)<< "Z" << " | " << setw(3) <<  C[i][j] << " | ";
                else
                    cout << setw(3)<< "x" << saveRow[i]+1 << " | " << C[i][j] << " | ";
            }
            else {
                cout<< setw(4)  << C[i][j] << setw(4) << " | ";
            }
        }
        cout << endl;
    }
    // #14 Table by method-rectangle
    for(i=0; i<=m; i++) {
        for(j=0; j<n+m+1; j++) {
            if(i != maxIndex && j != (razStolbec+1)) {
                //if(C[maxIndex][j] == zero) continue;
                cout <<endl<< "[" << i+1 << "]["<<j+1<<"]";
                cout << C[i][j] << " = " << C[i][j] << " - ("<< C[i][rc+1]<< " * " << C[maxIndex][j] << ")/"<<C[maxIndex][rc+1]<<"=";
                C[i][j] = C[i][j] - ( (C[i][rc+1]*C[maxIndex][j])/C[maxIndex][rc+1] );
                cout << C[i][j];
            }
        }
    }
    for(j=0; j<n+m+1; j++) {
        if(j!=rc+1) C[maxIndex][j] = C[maxIndex][j]/C[maxIndex][rc+1];
    }
    C[maxIndex][rc+1] = C[maxIndex][rc+1] / C[maxIndex][rc+1];
    for(i=0; i<=m; i++) {
        if(i != maxIndex)
            C[i][rc+1] = O;
    }


    //New B.P.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    saveRow[maxIndex]=rc;

    // #15 Output table

    cout << endl << endl << "SymplexTable: "<<  endl << setw(4) << "B.V." << setw(7) << " |  1  | ";
    for(i=0; i<n+m; i++) {
        cout <<setw(4)<< "x" << i+1 << " | ";
    }
    cout << endl;
    for(i=0; i<=m; i++) {
        for(j=0; j<n+m+1; j++) {
            if(j==0) {
                if(i==m)
                    cout << setw(4)<< "Z" << " | " << setw(3) <<  C[i][j] << " | ";
                else
                    cout << setw(3)<< "x" << saveRow[i]+1 << " | " << C[i][j] << " | ";
            }
            else {
                cout<< setw(3)  << C[i][j] << setw(4) << " | ";
            }
        }
        cout << endl;
    }

    // #16 while !!optimum
    int optimalPlan = 0, cnt, indexOfMaxBasis;
    do {
        // #16.1 Check basic for "+/-"
        cnt = 0;
        for (i=0; i<=m; i++) {
            if(C[i][0] > O) cnt++;
        }
        if (cnt == (m+1)) {
            optimalPlan = 1; //flag 1 optimal
            continue;
        }

        // #16.2 !!optimim, finding max |-a|
        Fraction maxBasis(0,1);
        indexOfMaxBasis = 0;
        for(i=0; i<=m; i++) {
            if(Abs(C[i][0]) > maxBasis && C[i][0] < O) {
                maxBasis = Abs(C[i][0]);
                indexOfMaxBasis = i;
            }
        }

 //       cout << "\nIomb: " << indexOfMaxBasis << endl;
        // #16.3 Min otnoshenie
        Fraction minOtnoshenie(1000,1),tempfrac(1,1);
        rc=0;
        for(j=1; j<n+m+1; j++) {
            if(C[m][j] == O || C[indexOfMaxBasis][j] == O) continue;
            tempfrac = C[m][j]/C[indexOfMaxBasis][j];
            cout << "\n tf:" << tempfrac << endl;
            if(tempfr < minOtnoshenie) {
                minOtnoshenie = tempfrac;
                rc = j;
                cout << "MinOt: " << minOtnoshenie << endl;
            }
        }
        cout << "\n R element: "<< C[indexOfMaxBasis][rc]<<endl;
        cout << "raz stol:" << rc << endl;
        saveRow[indexOfMaxBasis] = rc; //????????????????????????????????????????????????????
        cout << "test";
        for(i=0; i<=m; i++) {
            for(j=0; j<n+m+1; j++) {
              Ct[i][j]=C[i][j];
            }
          }
        // #16.4 Rectangle method
        for(i=0; i<=m; i++) {
            for(j=0; j<n+m+1; j++) {
                if(i != indexOfMaxBasis && j != (rc+1)) {
                    cout <<endl<< "[" << i+1 << "]["<<j+1<<"]";
                    cout << C[i][j] << " = " << Ct[i][j] << " - ("<< Ct[i][rc]<< " * " << Ct[indexOfMaxBasis][j] << ")/"<<Ct[indexOfMaxBasis][razStolbec]<<"=";
                    //if(C[indexOfMaxBasis][razStolbec] == zero ||(C[i][razStolbec]*C[indexOfMaxBasis][j]) == zero) continue;
                    C[i][j] = Ct[i][j] - ( (Ct[i][rc]*Ct[indexOfMaxBasis][j])/Ct[indexOfMaxBasis][rc] );
                    cout << C[i][j];
                }
            }
        }
        for(j=0; j<n+m+1; j++) {
            if(j!=rc) C[indexOfMaxBasis][j] = C[indexOfMaxBasis][j] / C[indexOfMaxBasis][rc];
        }
        C[indexOfMaxBasis][rc] = C[indexOfMaxBasis][rc+1] / C[indexOfMaxBasis][rc];
        for(i=0; i<=m; i++) {
            if(i != indexOfMaxBasis)
                C[i][rc] = O;
        }
        // #16.6 Output table
        cout << endl << endl  << "SymplexTable: "<<  endl << setw(4) << "B.P." << setw(7) << " |  1  | ";
        for(i=0; i<n+m; i++) {
            cout <<setw(4)<< "x" << i+1 << " | ";
        }
        cout << endl;
        for(i=0; i<=m; i++) {
            for(j=0; j<n+m+1; j++) {
                if(j==0) {
                    if(i==m)
                        cout << setw(4)<< "Z" << " | " << setw(3) <<  C[i][j] << " | ";
                    else
                        cout << setw(3)<< "x" << saveRow[i]+1 << " | " << C[i][j] << " | ";
                }
                else {
                    cout << "  " << setw(3) << C[i][j] << setw(2) << " | ";
                }
            }
            cout << endl;
        }

    } while(optimalPlan != 1);

    // #17 Выводим ответ
    int zc,ec,epos,zzz=0;
    Fraction result(0,1);
    Fraction oneFr(1,1);
    cout << endl << "Z = ";
    for(j=1; j<n+m+1; j++) {
        zc=0;
        ec=0;
        for(i=0; i<=m; i++) {
            if(C[i][j] == O) {
                zc++;
            } else if(C[i][j]== oneFr) { //E=???
                ec++;
                epos = i;
            }
        }
        if(zc == m && ec == 1) { //�� �� ���
            if(zzz >= n) break;
            cout << " " << Z[zzz] << "*"<<C[epos][0];
            if(zzz != n-1) cout << " + ";
            result = result + (C[epos][0]*Z[zzz]);
            zzz++;
        }
    }
    cout << " = " << result << endl << endl;
}

bool menu () {
    int n;
    printf("\n1) Dual simplex method\n");
    scanf("%d", &n);
    switch (n) {
    case 1:
        std::system("clear");
        DualSimplexMethod();
        break;

    default:
        printf("\nYour input is wrong!\n");
        break;
    }
    return true;
}

long long fact(long long n) {
    if (n <= 1)
        return 1;
    return n * fact (n - 1);
}

long long combinationNum(long long a, long long b) {
    if (a == b)
        return 1;
    return fact (b) / (fact (b - a) * fact (a));
}

void JordanGaussTwo(Fraction ** arr, int n, int m) {
    int i, j, y = 0, mx_ind = 0;
    bool check;
    Fraction mx, dx;
������ 
    for (i = 0; i < n; i++) {
        if (y > m - 1 || i == n - 1)
            break;
        check = false;
        mx_ind = y;
        mx = Fraction (0, 1);

        /* #1 Searching maximum element in column */
        for (j = y; j < m; j++) {
            if (dx.Fraction::Abs (arr[j][i]) > mx) {
                mx = dx.Fraction::Abs (arr[j][i]);
                mx_ind = j;
                check = true;
            }
        }
        /* #1 Searching maximum element in column */
        rowSwap(arr, n, y, mx_ind);
        if (!check)
            continue;

        /* #2 Recalculation of elements by the method of rectangles ignore the reference line */
        for (j = i + 1; j < n; j++) {
            for (int l = 0; l < m; l++) {
                if (l != y)
                    arr[l][j] = arr[l][j] - (arr[y][j] * arr[l][i]) / arr[y][i];
            }
        }
        /* #2 Recalculation of elements by the method of rectangles ignore the reference line */

        /* #3 Div row on base element */
        for (j = i + 1; j < n; j++) {
            arr[y][j] = arr[y][j] / arr[y][i];
        }

        arr[y][i] = arr[y][i] / arr[y][i];
        /* #3 Div row on base element */

        /* #4 Column to zero */
        for (j = 0; j < m; j++) {
            if (j != y)
                arr[j][i] = Fraction (0, 1);
        }
        /* #4 Column to zero */

        y++;

    }

    /* #5 Rang Calculating */
    int rang = 0;
    int cnt = 0;
    for (i = 0; i < m; i++) {
        cnt = 0;
        for (j = 0; j < n - 1; j++) {
            if (arr[i][j] == Fraction (1, 1))
                cnt++;
        }
        if (cnt == 1)
            rang++;
    }
    m = rang;
    /* #5 Rang Calculating */

    print (arr, n, m);

    /* #6 Generating all combinations */
    int combNum = combinationNum(rang, n - 1);
    int comb_ind = rang;
    int *tmp_comb = new int[rang];
    int *comb = new int[rang * combNum];
    for (i = 0; i < rang; i++) {
        tmp_comb[i] = i + 1;
        comb[i] = i + 1;
    }

    while (NextSet (tmp_comb, n - 1, rang)) {
        for (i = 0; i < rang; i++) {
            comb[comb_ind] = tmp_comb[i];
            comb_ind++;
        }
    }
    /* #6 Generating all combinations */

    cout << "Rang :" << rang << endl << "N: " << n -
         1 << endl << "Number of combinations: " << combNum << endl;


    Fraction ** temp = new Fraction *[m];
    int ed[rang][n] = {};
    int ch0[rang] = {};
    Fraction result;
    bool flag1, flag2, flagO;
    int counter = 0;

    for (i = 0; i < m; i++)
        temp[i] = new Fraction[n];

    copy(arr, temp, n, m);
    print (temp, n, m);
    cout << endl;
    int tempH=0;
    for (i = 0 ; i < rang * combNum; i++) {
        cout << comb[i] << " ";
        if(tempH==rang-1) {
            cout << endl;
            tempH=0;
        }
        else {
            tempH++;
        }
    }


    cout << endl;
    for (i = 0; i < rang; i++)
        ch0[i] = 0;

    for (int k = 0; k < rang * combNum; k++) { // Cycle to through all combinations
        int index = comb[k];
        flag1 = 0;
        flag2 = 0;
        flagO = 1;
        counter = 0;
        for (i = 0; i < rang; i++) {
            if (!(temp[i][index - 1] == Fraction (0, 1)) && ch0[i] == 0) {
                ch0[i] = 1;
                flag1 = 1;
                /* #7 Recalculation of elements by the method of rectangles ignore the reference line */
                for (j = 0; j < rang; j++) {
                    for (int l = 0; l < n; l++) {
                        if (i == j || index - 1 == l)
                            continue;
                        temp[j][l] = temp[j][l] - (temp[j][index - 1] * temp[i][l]) / temp[i][index - 1];
                    }
                }
                /* #7 Recalculation of elements by the method of rectangles ignore the reference line */

                /* #8 Div column on zero and div row on base element */
                for (j = 0; j < rang; j++) {
                    if (j != i)
                        temp[j][index - 1] = Fraction (0, 1);
                }
                for (j = 0; j < n; j++) {
                    if (j != index - 1)
                        temp[i][j] = temp[i][j] / temp[i][index - 1];

                }
                /* #8 Div column on zero and div row on base element */
                temp[i][index - 1] = Fraction (1, 1);
                ed[i][index - 1] = 1;
                counter++;
                print(temp, n, m);
            }

            if (flag1)
                break;
            if (i == rang - 1 && counter != rang)
                flag2 = 1;
        }
        if((k + 1) % rang == 0) {
            for (i = 0; i < rang; i++)
                ch0[i] = 0;
            cout << endl << "Basic Resolving #" << ((k - 1) / rang) + 1 << " ";
            cout << "[";
            for (i = 0; i < rang; i++)
                cout << comb[k - rang + i + 1] << ",";
            cout << "]";
            cout << endl;
            if (flag2 == 1) {
                cout << "No basic resolve";
                copy (arr, temp, n, m);
                cout << endl << "_________________________________________________________________________________" << endl;
                continue;
            }

            for (j = 0; j < n - 1; j++) {
                result = Fraction (0, 1);
                for (i = 0; i < rang; i++) {
                    if (ed[i][j] == 1)
                        result = temp[i][n - 1];
                    if (result < Fraction (0, 1))
                        flagO = 0;
                }
                if (!(result == Fraction (1, 1)))
                    cout << setw (3) << result << "; ";
                else
                    cout << 0 << " ";
            }

            if (!flagO)
                cout << "Ne ";
            cout << "opornoe!";
            cout << endl << "_________________________________________________________________________________" << endl;
            for (i = 0; i < rang; i++) {
                for (j = 0; j < n; j++) {
                    ed[i][j] = 0;
                }
            }
            copy (arr, temp, n, m);
        }
    }

}

void copy(Fraction ** from, Fraction ** to, int n, int m) {
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            to[i][j] = from[i][j];
}
